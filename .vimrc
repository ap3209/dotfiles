" Setting both enables hybrid line numbers — the current line is absolute, while the others are relative.
set number
set relativenumber

" bottom bar
set laststatus=2

" get rid for -- INSERT --
set noshowmode

" case insensitive search
set ignorecase
