# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

export PATH="$HOME/.window_manager_files_and_scripts/system_script:$PATH"

export PATH="$HOME/.portable_apps/platform-tools:$PATH"

### ALIASES ###
# navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# ls
alias ls='ls --color=auto'
alias ll='ls -lah'

# transmission
alias tmd='transmission-daemon'
alias tmr='transmission-remote'
alias tmre='transmission-remote --exit'
alias tmrw='watch -n7 transmission-remote -l'

# apt
alias aupdate='sudo apt update'
alias asearch='apt search'
alias apolicy='apt policy'
alias ainfo='apt info'
alias alupgradable='apt list --upgradable'
alias aupgrade='sudo apt dist-upgrade -y && flatpak update -y'

#PS1=" \[[33m\] \[[00m\]:\[[36m\]\w\[[00m\] "

PS1='\[\e[92m\]┌──\[\e[92m\](\[\e[97m\] \[\e[0m\]👹\[\e[38;5;226m\] \[\e[92m\])\[\e[92m\]-\[\e[92m\][\[\e[38;5;45;1m\]\w\[\e[0;92m\]]\n\[\e[92m\]└─\[\e[0m\]\$ '
